#include <stdio.h>

#include "Barometer.h"

#include <Wire.h>
#include <utility/twi.h>

// #include "SoftwareSerial.h"
// #include "SerialLCD.h"
// #include "BigSerialLCD.h"

// #include "DHT.h"

// #define LEDB 5
// #define BTN1 2
// #define BTN2 3
// #define DHTTYPE DHT11
// #define DHTPIN 6

// дисплей
//BigSerialLCD slcd(7,8);
// список подключенных по i2c устройств
bool i2cDevices[128] = {false};
// всё для датчика давления
float temperature;
float pressure;
float atm;
float altitude;
Barometer myBarometer;
// датчик влажности
//DHT dht(DHTPIN, DHTTYPE);
// асинхронность
unsigned long lastPresTempUpdate = 0;
unsigned long currentTime;

uint8_t getI2CDevices(uint8_t array[], uint8_t arrayLen) {
	uint8_t nDevices = 0;
  for(uint8_t address = 0; address <= 127; address++) {
      Wire.beginTransmission(address);
      uint8_t error = Wire.endTransmission();
          if (error == 0) { // устройство найдено
            // digitalWrite(LEDB, HIGH);
            // delay(100);
            // digitalWrite(LEDB, LOW);
            array[nDevices++] = address;
          }
  }
  return nDevices;
}

void setup() {
  Serial.begin(9600);
  //dht.begin();
  // slcd.begin();
  // slcd.backlight();
  // Serial.print("1");
  myBarometer.init();
  // Serial.print("2");
  // Wire.begin();
  // Serial.print("3");
  // pinMode(LEDB, OUTPUT);

  uint8_t array[16] = {0};
  uint8_t nDevices = getI2CDevices(array, 16);
  // for(uint8_t i=0; i<nDevices; i++) {
  //   slcd.print((unsigned long)array[i], DEC);
  //   slcd.print(" ");
  //   Serial.print((int)array[i]);
  //   Serial.print(" ");
  // }

  if(nDevices == 0) {
    // digitalWrite(LEDB, HIGH); 
    // slcd.print("no i2c devices found");
  }
}

void loop() {
  currentTime = millis();

  if(currentTime - lastPresTempUpdate >= 1000) {
    temperature = myBarometer.bmp085GetTemperature(myBarometer.bmp085ReadUT()); //Get the temperature, bmp085ReadUT MUST be called first
    pressure = myBarometer.bmp085GetPressure(myBarometer.bmp085ReadUP());//Get the temperature
    altitude = myBarometer.calcAltitude(pressure); //Uncompensated caculation - in Meters 
    atm = pressure / 101325; 

    // float h = dht.readHumidity();
    // float t = dht.readTemperature();

    // slcd.setCursor(0, 1); slcd.print("pres ");  slcd.print(pressure, 2);
    // slcd.setCursor(0, 2); slcd.print("temp ");  slcd.print(temperature, 2); slcd.print(" "); slcd.print(t, 2);
    // slcd.setCursor(0,3);  slcd.print("humid "); slcd.print(h, 2);

    Serial.print("pressure: "); Serial.print(pressure); Serial.print("\n");
    Serial.print("temperature: "); Serial.print(temperature); Serial.print("\n\n");
    // Serial.print("humidity: "); Serial.print(h); Serial.print("\n");

    lastPresTempUpdate = currentTime;
  }

	// if(digitalRead(BTN1) == HIGH) {
	// 	slcd.scrollDisplayUp();
	// 	delay(120);
	// }	
	// if(digitalRead(BTN2) == HIGH) {
	// 	slcd.scrollDisplayDown();
	// 	delay(120);
	// }
}