/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  This example code is in the public domain.
 */

// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
#define LED1 13
#define LED2 12
#define BUTTON 7
#define SPIN 8

unsigned long currentTime = 0;
unsigned long stopTime = 0;

double getCelsiusTemp(int pinId){
  double val = analogRead(pinId);
  double resistance = (double)(1023 - val)*10000/val;
  double B = 3975;
  return 1/(log(resistance/10000)/B+1/298.15)-273.15;
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
      Serial.begin(9600);
      pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED2, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(LED2, digitalRead(BUTTON));
  analogWrite(LED1, analogRead(SPIN)/4.0);
  currentTime = millis();
  if (abs(currentTime-stopTime)>1000){
    Serial.print("Current time: ");
    Serial.println(currentTime);
    Serial.print("Stop    time: ");
    Serial.println(stopTime);
    Serial.print("Current temp is: ");
    Serial.println(getCelsiusTemp(A0));
    stopTime=currentTime;

  }  
//  delay(10);
}
