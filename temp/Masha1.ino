/**********************************************************************************
 DESCRIPTION:
  An illumination for New Year

CONFIGURATION:
  board: UNO,
  8 channable RGB LEDs to pin D5
  
***********************************************************************************/

#include <ChainableLED.h>

#define BVM_SIZE_CTX         8

#define BVM_REG_ACC          0
#define BVM_REG_PRM1         1
#define BVM_REG_PRM2         2
#define BVM_REG_PRM3         3
#define BVM_REG_IP           4
#define BVM_REG_SP           5

#define NUM_LEDS             8

#define QTY_PROCESSES        2

#define BVM_CODE_NOP         0
#define BVM_CODE_GOTO        1
#define BVM_CODE_CLR         2
#define BVM_CODE_LDI         3
#define BVM_CODE_SETPRM1     4
#define BVM_CODE_SETPRM2     5
#define BVM_CODE_SETPRM3     6
#define BVM_CODE_HALT        7
#define BVM_CODE_PUSH        8
#define BVM_CODE_POP         9
#define BVM_CODE_DISCARD     10
#define BVM_CODE_PUSHIP      11
#define BVM_CODE_POPIP_NZ    12
#define BVM_CODE_DEC         13
#define BVM_CODE_CALL        14
#define BVM_CODE_RET         15

#define BVM_CODE_SLEEP       16

#define BVM_CODE_LED_SETRGBI 17
#define BVM_CODE_LED_SETRGBA 18


#define BVM_CODE_LABEL       254
///

#define BVM_CODE_SET_DELTA   2

ChainableLED leds1(5, 6, NUM_LEDS);

extern unsigned char bvm_seg_code[256];
extern unsigned char bvm_seg_stack[256];

void bvm_ni_nop(unsigned char *ctx){
}

void bvm_ni_goto(unsigned char *ctx){
  ctx[BVM_REG_IP] = ctx[BVM_REG_ACC];
}

void bvm_ni_clr(unsigned char *ctx){
  ctx[BVM_REG_ACC] = 0;
}

void bvm_ni_ldi(unsigned char *ctx){
  ctx[BVM_REG_ACC] = bvm_seg_code[ctx[BVM_REG_IP]++];
}

void bvm_ni_setprm1(unsigned char *ctx){
  ctx[BVM_REG_PRM1] = ctx[BVM_REG_ACC];
}

void bvm_ni_setprm2(unsigned char *ctx){
  ctx[BVM_REG_PRM2] = ctx[BVM_REG_ACC];
}

void bvm_ni_setprm3(unsigned char *ctx){
  ctx[BVM_REG_PRM3] = ctx[BVM_REG_ACC];
}

void bvm_ni_push(unsigned char *ctx){
  bvm_seg_stack[ctx[BVM_REG_SP]--] = ctx[BVM_REG_ACC];
}

void bvm_ni_pop(unsigned char *ctx){
  ctx[BVM_REG_ACC] = bvm_seg_stack[++ctx[BVM_REG_SP]];
}

void bvm_ni_discard(unsigned char *ctx){
  ++ctx[BVM_REG_SP];
}

void bvm_ni_puship(unsigned char *ctx){
  bvm_seg_stack[ctx[BVM_REG_SP]--] = ctx[BVM_REG_IP] - 1;
}

void bvm_ni_popip_nz(unsigned char *ctx){
  char n;

  n = bvm_seg_stack[++ctx[BVM_REG_SP]];

  if(ctx[BVM_REG_ACC])
    ctx[BVM_REG_IP] = n;
}

void bvm_ni_dec(unsigned char *ctx){
  --ctx[BVM_REG_ACC];
}

void bvm_ni_call(unsigned char *ctx){
  unsigned char i, n, icode, label;

  bvm_seg_stack[ctx[BVM_REG_SP]--] = ctx[BVM_REG_IP];
  
  n = ctx[BVM_REG_ACC];
  
  for(i = 0; i < 255; i++){
    icode = bvm_seg_code[i];
    label = bvm_seg_code[i + 1];

    if((icode == BVM_CODE_LABEL) && (label == n)){
      ctx[BVM_REG_IP] = i + 2;
      return;
    }
  }
  
  bvm_ni_discard(ctx);
}

void bvm_ni_ret(unsigned char *ctx){
  ctx[BVM_REG_IP] = bvm_seg_stack[++ctx[BVM_REG_SP]];
}

void bvm_ni_sleep(unsigned char *ctx){
  delay(ctx[BVM_REG_ACC]);
}

void bvm_ni_led_setrgbi(unsigned char *ctx){
  leds1.setColorRGB(ctx[BVM_REG_ACC],
    ctx[BVM_REG_PRM1], ctx[BVM_REG_PRM2], ctx[BVM_REG_PRM3]);
}

void bvm_ni_led_setrgba(unsigned char *ctx){
  unsigned char i;
  for(i = 0; i < NUM_LEDS; i++)
    leds1.setColorRGB(i,
      ctx[BVM_REG_PRM1], ctx[BVM_REG_PRM2], ctx[BVM_REG_PRM3]);
}

void (*bvm_table_ni[32])(unsigned char*) = {
  bvm_ni_nop,
  bvm_ni_goto,
  bvm_ni_clr,
  bvm_ni_ldi,
  bvm_ni_setprm1,
  bvm_ni_setprm2,
  bvm_ni_setprm3,
  bvm_ni_nop, /* halt entry, ignored */
  bvm_ni_push,
  bvm_ni_pop,
  bvm_ni_discard,
  bvm_ni_puship,
  bvm_ni_popip_nz,
  bvm_ni_dec,
  bvm_ni_call,
  bvm_ni_ret,
  bvm_ni_sleep,
  bvm_ni_led_setrgbi,
  bvm_ni_led_setrgba
};

unsigned char bvm_seg_code[256] = {
  // loop - deeming green phase
    BVM_CODE_LDI, 32, // maximal green
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,  // decrease green at each iteration
    BVM_CODE_PUSH, // save intensity to stack

    BVM_CODE_CLR,
    BVM_CODE_SETPRM3,
    BVM_CODE_POP,  // peek green intensity
    BVM_CODE_PUSH,
    BVM_CODE_SETPRM2,
    BVM_CODE_CLR,
    BVM_CODE_SETPRM1,
    BVM_CODE_LED_SETRGBA,

    BVM_CODE_LDI, 1,
    BVM_CODE_SLEEP,

    BVM_CODE_POP,   // restore green intensity
    BVM_CODE_POPIP_NZ,
  // end of deeming green phase

  // loop - deeming blue phase
    BVM_CODE_LDI, 32, // maximal blue
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,  // decrease blue at each iteration
    BVM_CODE_PUSH, // save intensity to stack

    BVM_CODE_POP,  // peek blue intensity
    BVM_CODE_PUSH,
    BVM_CODE_SETPRM3,
    BVM_CODE_CLR,
    BVM_CODE_SETPRM2,
    BVM_CODE_CLR,
    BVM_CODE_SETPRM1,
    BVM_CODE_LED_SETRGBA,

    BVM_CODE_LDI, 1,
    BVM_CODE_SLEEP,

    BVM_CODE_POP,   // restore blue intensity
    BVM_CODE_POPIP_NZ,
  // end of deeming blue phase

  // pink phase
  BVM_CODE_LDI, 200,
  BVM_CODE_SETPRM3,
  BVM_CODE_LDI, 200,
  BVM_CODE_SETPRM2,
  BVM_CODE_LDI, 255,
  BVM_CODE_SETPRM1,
  BVM_CODE_CLR,
  BVM_CODE_LED_SETRGBA,

  BVM_CODE_LDI, 255,
  BVM_CODE_SLEEP,
  BVM_CODE_SLEEP,
  BVM_CODE_SLEEP,

  // loop 8..1  - avalanche change to orange
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to orange
      BVM_CODE_PUSH,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 128,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 192,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 150 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 150,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1

  // loop 8..1  - avalanche change to cyan
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to cyan
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 90,
      BVM_CODE_SETPRM3,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 90,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 150 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 150,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1

  // loop 8..1  - avalanche change to ?
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to ?
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 96,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 96,
      BVM_CODE_SETPRM2,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 150 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 150,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1

  BVM_CODE_LDI, 0, // calling subr - set blue
  BVM_CODE_CALL,

//  BVM_CODE_LDI, 192,
//  BVM_CODE_SETPRM3,
//  BVM_CODE_LDI, 0,
//  BVM_CODE_SETPRM2,
//  BVM_CODE_LDI, 0,
//  BVM_CODE_SETPRM1,
//  BVM_CODE_CLR,
//  BVM_CODE_LED_SETRGBA,
  
  BVM_CODE_LDI, 255,
  BVM_CODE_SLEEP,
  BVM_CODE_SLEEP,
  BVM_CODE_SLEEP,

  // loop 8..1  
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to red
      BVM_CODE_PUSH,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM3,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 192,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 150 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 150,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,

    // set led i to blue
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 64,
      BVM_CODE_SETPRM3,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM2,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1
  
  BVM_CODE_LDI, 32,
  BVM_CODE_SETPRM3,
  BVM_CODE_CLR,
  BVM_CODE_SETPRM2,
  BVM_CODE_LDI, 32,
  BVM_CODE_SETPRM1,
  BVM_CODE_CLR,
  BVM_CODE_LED_SETRGBA,

  // loop 8..1  
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to ...
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 128,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 192,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 255,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 255 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 255,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,

    // set led i to purple
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 32,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 32,
      BVM_CODE_SETPRM2,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1

  BVM_CODE_CLR,
  BVM_CODE_SETPRM3,
  BVM_CODE_LDI, 32,
  BVM_CODE_SETPRM2,
  BVM_CODE_LDI, 32,
  BVM_CODE_SETPRM1,
  BVM_CODE_CLR,
  BVM_CODE_LED_SETRGBA,

  // loop 8..1  
    BVM_CODE_LDI, 8,
    BVM_CODE_PUSHIP,
    BVM_CODE_DEC,
    
    // set led i to ...
      BVM_CODE_PUSH,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 192,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 192,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    // wait 255 ms
      BVM_CODE_PUSH,
      BVM_CODE_LDI, 255,
      BVM_CODE_SLEEP,
      BVM_CODE_POP,

    // set led i to purple
      BVM_CODE_PUSH,
      BVM_CODE_CLR,
      BVM_CODE_SETPRM3,
      BVM_CODE_LDI, 32,
      BVM_CODE_SETPRM2,
      BVM_CODE_LDI, 32,
      BVM_CODE_SETPRM1,
      BVM_CODE_POP,
      BVM_CODE_LED_SETRGBI,
      
    BVM_CODE_POPIP_NZ,
  // end of loop 8..1

  BVM_CODE_CLR,
  BVM_CODE_GOTO,

  // set all to RGB  
  BVM_CODE_LABEL, 0,
    BVM_CODE_LDI, 64,
    BVM_CODE_SETPRM3,
    BVM_CODE_CLR,
    BVM_CODE_SETPRM2,
    BVM_CODE_CLR,
    BVM_CODE_SETPRM1,
    BVM_CODE_CLR,
    BVM_CODE_LED_SETRGBA,
  BVM_CODE_RET,
};

unsigned char bvm_seg_data[256];
unsigned char bvm_seg_stack[256];

void bvm_loop(unsigned char *ctx){
  unsigned char icode, ip;

  while(1){
    ip = ctx[BVM_REG_IP]++;
    icode = bvm_seg_code[ip];
    
    if(icode == BVM_CODE_HALT)
      return;

    bvm_table_ni[icode](ctx);
  }
}

unsigned char bvm_main(void){
  unsigned char ctx[BVM_SIZE_CTX], i;
  
  for(i = 0; i < BVM_SIZE_CTX; i++)
    ctx[i] = 0;

  ctx[BVM_REG_SP] = 255;
  
  bvm_loop(ctx);

  return ctx[BVM_REG_ACC];
}

unsigned char vm_instruction_pointer[QTY_PROCESSES] = {0, 0};
unsigned char vm_delay[QTY_PROCESSES];

unsigned char vm_process_current = 0;

void setup()
{
}

float hue1 = 0.99;
boolean up1 = true;

void loop()
{
  bvm_main();
  
  for (byte i=0; i<NUM_LEDS; i++){
    leds1.setColorHSB(i, hue1, 1.0, 0.5);
  }    
  delay(100);
    
  if (up1)
    hue1+= 0.025;
  else
    hue1-= 0.025;
    
  if (hue1>=1.0 && up1)
    up1 = false;
  else if (hue1<=0.0 && !up1)
    up1 = true;
}
