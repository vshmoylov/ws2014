#include <Streaming.h>

unsigned long currentTime = 0;
unsigned long stopTime = 0;

double getCelsiusTemp(int pinId){
  double val = analogRead(pinId);
  double resistance = (double)(1023 - val)*10000/val;
  double B = 3975;
  return 1/(log(resistance/10000)/B+1/298.15)-273.15;
}

void setup() {
  Serial.begin(9600);
}

void loop() {
  currentTime = millis();
  if (abs(currentTime-stopTime)>=1000){
    Serial << getCelsiusTemp(A0) << endl;
    stopTime=currentTime;
  }
}
