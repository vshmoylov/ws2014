#-------------------------------------------------
#
# Project created by QtCreator 2014-02-02T16:51:40
#
#-------------------------------------------------

QT       += core gui

CONFIG += serialport

TARGET = untitled1
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
