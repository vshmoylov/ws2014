#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtCore>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void receiveData();
    
    void on_pushButton_clicked();

private:
    Ui::Widget *ui;
    QTimer *timer;
    QList<QSerialPortInfo> ports;
    int currentPort;
    QSerialPort serial;
    QFile logFile;
    int cntr;
};

#endif // WIDGET_H
