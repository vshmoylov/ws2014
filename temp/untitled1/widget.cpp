#include "widget.h"
#include "ui_widget.h"
#include <QTimer>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(receiveData()));

    ports = QSerialPortInfo::availablePorts();
    foreach (const QSerialPortInfo &info, ports) {
        ui->comboBox->addItem(info.portName());
    }

}

Widget::~Widget()
{
    serial.close();
    delete ui;
}

void Widget::receiveData()
{

    if (serial.isOpen()){

        QByteArray data = serial.readAll();
        QString str = QString::fromLocal8Bit(data).trimmed();
        if (str.isEmpty()) return;
        cntr++;
        ui->lcdNumber->display(str.toDouble());
        str.replace('.',',');
        QString toWrite;
        toWrite = QString("%1;%2\n").arg(str).arg(QTime::currentTime().toString("hh:mm:ss"));
        if (logFile.open(QIODevice::Append|QIODevice::Text)){
            logFile.write(toWrite.toLocal8Bit());
            logFile.close();
        }
        ui->label->setText(QString::fromUtf8("Измерений: %1").arg(cntr));
    }
}

void Widget::on_pushButton_clicked()
{
    timer->start(500);
    ui->pushButton->setEnabled(false);
    currentPort = ui->comboBox->currentIndex();
    serial.setPort(ports[currentPort]);
    serial.open(QIODevice::ReadOnly);
    logFile.setFileName("d:\\log.csv");
    cntr=0;
}
