#ifndef DEBUG_H
#define DEBUG_H

#define LCDPIN0 8
#define LCDPIN1 9


#ifdef DEBUG
SerialLCD lcd(LCDPIN0, LCDPIN1);
#define Debug_print(X...) lcd.print(X)
#define Debug_clear() { lcd.clear(); lcd.setCursor(0,0); }
#define Debug_init() { lcd.begin(); lcd.print("ready"); lcd.setCursor(0,1); /*lcd.backlight();*/ }
#define Debug_setCursor(X, Y) lcd.setCursor(X, Y)
#endif

#ifndef DEBUG
#define Debug_print(X...)
#define Debug_clear()
#define Debug_init()
#define Debug_setCursor(X, Y)
#endif


#endif//DEBUG_H