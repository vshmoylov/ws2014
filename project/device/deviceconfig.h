#ifndef DEVICECONFIG_H
#define DEVICECONFIG_H

PinState pinConfig[PINCOUNT];
byte bufPinConfig[64]={0};

void loadConfig(){
}

void applyConfig(){
    for (uint8_t i=0; i<PINCOUNT; i++){
        if (pinConfig[i].id){
            pinMode(i, pinConfig[i].output?OUTPUT:INPUT);
        }
    }
    for (uint8_t i=0; i<PINCOUNT; i++){
        bufPinConfig[i]=pinStateToByte(pinConfig[i]);
    }
}

void setConfig(uint8_t receivedConfig[]){
    for (uint8_t i=0; i<PINCOUNT; i++){
        pinConfig[i] = fromByte(receivedConfig[i]);
    }
    applyConfig();
}



#endif//DEVICECONFIG_H