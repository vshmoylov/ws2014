#ifndef DATATRANSMITTER_H
#define DATATRANSMITTER_H

// #define DCHP_ENABLED

#ifdef ETHERNET
#define SERVER_PORT 8000
EthernetServer server(SERVER_PORT);
EthernetClient client;
#endif

bool hasData();
void initDataTransmitter();
uint8_t recvPacket(uint8_t buf[]);
uint8_t sendPacket(uint8_t buf[]);
uint8_t *createPacket(uint8_t command, uint8_t *args, uint8_t arglength);


#ifdef ETHERNET
    void initDataTransmitter(){
        byte mac[] = {
            0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
        };
        IPAddress ip(192, 168, 1, 50);
        IPAddress gateway(192, 168, 1, 1);
        IPAddress subnet(255, 255, 255, 0);
    #ifdef DCHP_ENABLED
        if (Ethernet.begin(mac) == 0) 
    #endif//DCHP_ENABLED
        {//failed to configure ip using dhcp, fallinf back to defaults
            Ethernet.begin(mac, ip, gateway, subnet);
        }
        server.begin();
    #ifdef DCHP_ENABLED
        ip = Ethernet.localIP();
        Debug_print("ip: ");
        Debug_print(ip[0], DEC);
        Debug_print(".");
        Debug_print(ip[1], DEC);
        Debug_print(".");
        Debug_print(ip[2], DEC);
        Debug_print(".");
        Debug_print(ip[3], DEC);
    #endif//DCHP_ENABLED
    }
    bool hasData(){
        client = server.available();
        if (client && client.available()>0)
            return true;
        else 
            return false;
    }
    uint8_t recvPacket(uint8_t buf[]){
        uint8_t result = client.readBytes((char*)buf, PACKETSIZE);
        return result;
    }
    uint8_t sendPacket(uint8_t buf[]){
        uint8_t result = client.write(buf, PACKETSIZE);
        return result;
    }
#else//ETHERNET not defined, falling back to Serial port
    void initDataTransmitter(){
        Serial.begin(SERIALSPEED);
    }
    bool hasData(){
        if (Serial.available()>0)
            return true;
        else
            return false;
    }
    uint8_t recvPacket(uint8_t buf[]){
        uint8_t result = Serial.readBytes((char*)buf, PACKETSIZE);
        return result;
    }
    uint8_t sendPacket(uint8_t buf[]){
        uint8_t result = Serial.write(buf, PACKETSIZE);
        return result;
    }
#endif//ETHERNET



uint8_t *createPacket(uint8_t command, uint8_t *args, uint8_t arglength){
    static uint8_t buf[PACKETSIZE];
    memset(buf, 0, PACKETSIZE);
    buf[0]=command;
    buf[OFFSET_VER]=1;
    memcpy(buf+1, args, min(arglength, PACKETSIZE-2));
    return buf;
}

#endif//DATATRANSMITTER_H