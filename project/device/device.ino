#include <SoftwareSerial.h>
#include <SerialLCD.h>
#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>
#include <DHT.h>
#include "Barometer.h"
#include "../host/types.h"
#include "deviceconfig.h"
#define DEBUG
#include "debug.h"
#define ETHERNET
#include "datatransmitter.h"

void setup() {
    loadConfig();
    applyConfig();
    Debug_init();
    initDataTransmitter();
}

uint8_t * handleDigitalRead(uint8_t *args);
uint8_t * handleDigitalWrite(uint8_t *args);
uint8_t * handleAnalogRead(uint8_t *args);
uint8_t * handleAnalogWrite(uint8_t *args);
uint8_t * handleGetConfig(uint8_t *args);
uint8_t * handleSetConfig(uint8_t *args);
uint8_t * handleGetDataFromDHT(uint8_t* args);
uint8_t * handleGetBarometerData(uint8_t* args);
uint8_t * createPacket(uint8_t command, uint8_t *args, uint8_t arglength);

void loop() {
    if (hasData()){
        Debug_clear();
        Debug_print(client.available(), DEC);
        Debug_print(" ");
        uint8_t currentPacket[PACKETSIZE]={0};
        recvPacket(currentPacket);
        uint8_t command = currentPacket[OFFSET_CMD];
        uint8_t *args = currentPacket+OFFSET_ARG;
        // uint8_t protocol = currentPacket[OFFSET_VER];//for future use
        Debug_print(command, DEC);
        uint8_t *response=NULL;

        switch (command) {
            case DigitalRead:
                Debug_print(" DR ");
                response = handleDigitalRead(args);
                break;
            case DigitalWrite:
                Debug_print(" DW ");
                response = handleDigitalWrite(args);
                break;
            case AnalogRead:
                Debug_print(" AR ");
                response = handleAnalogRead(args);
                break;
            case AnalogWrite:
                Debug_print(" AW ");
                response = handleAnalogWrite(args);
                break;
            case GetPinConfig:
                Debug_print(" GC ");
                response = handleGetConfig(args);
                break;
            case SetPinConfig:
                Debug_print(" SC ");
                response = handleSetConfig(args);
                break;
            case GetDataFromDHT:
                Debug_print(" DHT ");
                response = handleGetDataFromDHT(args);
                break;
            case GetBarometerData:
                Debug_print(" Barom ");
                response = handleGetBarometerData(args);
                break;
            case GetI2CDevicesList:
                Debug_print(" I2Clist ");
                response = handleGetI2CDevicesList(args);
                break;
        }
        if (response == NULL)
        response = createPacket(command, NULL, 0);
        sendPacket(response);
    }
}

uint8_t * handleDigitalRead(uint8_t *args){
    uint8_t pin = args[OFFSET_ARG_PIN];
    pinMode(pin, INPUT);
    Debug_print(pin, DEC);
    uint8_t value = digitalRead(pin);
    return createPacket(DigitalRead, &value, 1);
}

uint8_t * handleDigitalWrite(uint8_t *args){
    uint8_t pin   = args[OFFSET_ARG_PIN];
    uint8_t value = args[OFFSET_ARG_VALUE];
    pinMode(pin, OUTPUT);
    Debug_print(pin, DEC);
    Debug_print(":");
    Debug_print(value, DEC);
    digitalWrite(pin, value);
    return createPacket(DigitalWrite, NULL, 0);
}

uint8_t * handleAnalogRead(uint8_t *args){
    uint8_t pin = args[OFFSET_ARG_PIN];
    pinMode(pin, INPUT);
    Debug_print(pin, DEC);
    uint16_t value = analogRead(pin);
    return createPacket(AnalogRead, (uint8_t*)&value, 2);
}
uint8_t * handleAnalogWrite(uint8_t *args){
    uint8_t pin   = args[OFFSET_ARG_PIN];
    uint8_t value = args[OFFSET_ARG_VALUE];
    pinMode(pin, OUTPUT);
    Debug_print(pin, DEC);
    Debug_print(":");
    Debug_print(value, DEC);
    analogWrite(pin, value);
    return createPacket(AnalogWrite, NULL, 0);
}
uint8_t * handleGetDataFromDHT(uint8_t* args) {
    uint8_t pin = args[0], type = args[1];
    DHT dht(pin, type);
    dht.begin();
    float data[2];
    data[0] = dht.readHumidity();
    data[1] = dht.readTemperature();
    return createPacket(GetDataFromDHT, (uint8_t*)data, 8);
}
uint8_t * handleGetBarometerData(uint8_t* args) {
    class Barometer myBar;
    myBar.init();

    float data[3];
    data[0] = myBar.bmp085GetTemperature(myBar.bmp085ReadUT());
    data[1] = myBar.bmp085GetPressure(myBar.bmp085ReadUP()) / 133.3224f;
    data[2] = myBar.calcAltitude(data[1]);
    return createPacket(GetBarometerData, (uint8_t*)data, 12);
}
uint8_t * handleGetI2CDevicesList(uint8_t* args) {
    Wire.begin();
    uint8_t buf[16];
    uint8_t nDevices = 0;
    for(uint8_t address = 0; address <= 127; address++) {
        Wire.beginTransmission(address);
        uint8_t error = Wire.endTransmission();
        if (error == 0) { // устройство найдено
            buf[nDevices++] = address;
        }
    }
    return createPacket(GetI2CDevicesList, buf, nDevices);
}
uint8_t * handleGetConfig(uint8_t *args){
    return createPacket(GetPinConfig, bufPinConfig, PINCOUNT);
}

uint8_t * handleSetConfig(uint8_t *args){
    setConfig(args);
    return createPacket(SetPinConfig, NULL, 0);
}

uint8_t pinStateToByte(PinState state) {
    return ((state.id & 0b00111111) << 2) | ((state.analog & 1) << 1) | (state.output & 1);
}

PinState fromByte(uint8_t value){
    PinState state;
    state.id = value >> 2;
    state.analog = (value > 1)&1;
    state.output = value & 1;
    return state;
}
