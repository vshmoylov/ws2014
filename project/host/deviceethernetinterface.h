#ifndef DEVICEETHERNETINTERFACE_H
#define DEVICEETHERNETINTERFACE_H

#include "deviceinterface.h"
#include <QTcpSocket>

class DeviceEthernetInterface : public DeviceInterface
{
public:
    DeviceEthernetInterface();
    ~DeviceEthernetInterface();

    void connect(const QHostAddress &address, quint16 port);
    void connect(const     QString &hostName, quint16 port);
    void disconnect();
    bool isOpen() const;

private:
    QTcpSocket socket;
    QByteArray sendRecvPacket(QByteArray buf);
};

#endif // DEVICEETHERNETINTERFACE_H
