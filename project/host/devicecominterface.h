#ifndef DEVICECOMINTERFACE_H
#define DEVICECOMINTERFACE_H

#include "deviceinterface.h"
#include <QSerialPort>
#include <QSerialPortInfo>

class DeviceComInterface : public DeviceInterface
{
public:
    DeviceComInterface();
    ~DeviceComInterface();

    void connect(const QSerialPortInfo &info);
    void disconnect();
    bool isOpen() const;

private:
    QSerialPort comPort;
    QByteArray sendRecvPacket(QByteArray buf);
};

#endif // DEVICECOMINTERFACE_H
