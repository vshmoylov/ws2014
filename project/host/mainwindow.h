#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QtCore>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QTreeWidget>
#include "types.h"
#include "deviceinterface.h"
#include "stringutil.h"

#include <QVector>
#include <QCheckBox>
#include <QComboBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_openButton_clicked();
    void on_pushButton_clicked();
    void on_tabWidget_currentChanged(int index);
    void on_analogDataRefreshButton_clicked();
    void refreshI2CDevicesData();
    void setHighVoltage();
    void setLowVoltage();
    void on_pushButton_4_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_treeWidget_itemExpanded(QTreeWidgetItem *item);

    void on_pushButtonAnalogActuatorsRefresh_clicked();

    void on_connectionTypeComboBox_currentIndexChanged(int index);

    void on_digitalDataRefreshPushButton_clicked();

    void on_comPortsRefreshPushButton_clicked();

private:
    void getDeviceConfig();
    Ui::MainWindow *ui;
    QList<QSerialPortInfo> ports;
    DeviceInterface *device;
    QTimer *receiveTimer;
    QList<PinState> devConfig;

    QVector<QCheckBox *> output;
    QVector<QCheckBox *> analog;
    QVector<QComboBox *> pinType;

    QTimer *i2cTimer;
    void addI2CDeviceWidgetChilds(QTreeWidgetItem *a, I2CDevice i2c);
};

#endif // MAINWINDOW_H
