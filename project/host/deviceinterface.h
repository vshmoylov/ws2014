#ifndef DEVICEINTERFACE_H
#define DEVICEINTERFACE_H

#include <QList>
#include <QByteArray>
#include <QTime>
#include <QPair>
#include "types.h"

#define READWRITETIMEOUT 1000
#define READWAITTIME 10000
#define PROTOCOLVERSION 1

class DeviceInterface
{
public:
    enum ErrorCodes {
        NoError=0,
        DeviceNotOpenedError=1,
        UnknownError=2
    };
    enum DigitalWriteValues { LOW=0, HIGH=1 };

public:
    DeviceInterface();
    virtual ~DeviceInterface();

    //connection-related functions
//    virtual void connect()=0;
    virtual void disconnect() = 0;
    virtual bool isOpen() const = 0;

    //basic read/write functions
    int digitalRead(uint8_t pin, int *errCode=0);
    void digitalWrite(uint8_t pin, uint8_t value, int *errCode=0);
    int analogRead(uint8_t pin, int *errCode=0);
    void analogWrite(uint8_t pin, uint8_t value, int *errCode=0);

    //advanced read/write
    QPair<float, float> getDataFromDHT(uint8_t pin, int *errCode=0);
    QList<float> getBarometerData(int *errCode=0);

    //device configuration related functions
    QList<PinState> getPinConfig(int *errCode=0);
    void setPinConfig(const QList<PinState> &states, int *errCode =0);
    QList<I2CDevice> getI2CdevicesList(int *errCode=0);

private:
    virtual QByteArray sendRecvPacket(QByteArray buf)=0;
    QByteArray createCommandBuffer(CommandsEnum command, const QByteArray &arguments = QByteArray()) const;
    QByteArray createCommandBuffer(CommandsEnum command, const uint8_t *args, uint8_t argsLength) const;
    void handleErrCode(int *errCode) const;

};

#endif // DEVICEINTERFACE_H
