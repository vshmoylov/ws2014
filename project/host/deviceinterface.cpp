#include "deviceinterface.h"
#include <QDebug>


DeviceInterface::DeviceInterface()
{
}

DeviceInterface::~DeviceInterface()
{
}

int DeviceInterface::digitalRead(uint8_t pin, int *errCode)
{
    handleErrCode(errCode);
    if (!isOpen()) return -1;

    QByteArray resp = sendRecvPacket(createCommandBuffer(DigitalRead, &pin, 1));
    uint8_t result = *((uint8_t*)resp.data()+1);
    return result;
}

void DeviceInterface::digitalWrite(uint8_t pin, uint8_t value, int *errCode)
{
    handleErrCode(errCode);
    if (!isOpen()) return;

    QByteArray args(2, 0);
    args[OFFSET_ARG_PIN]=pin;
    args[OFFSET_ARG_VALUE]=value;
    QByteArray resp = sendRecvPacket(createCommandBuffer(DigitalWrite,args));
    if (resp.isEmpty() && errCode)
        *errCode=UnknownError;
}

int DeviceInterface::analogRead(uint8_t pin, int *errCode)
{
    handleErrCode(errCode);
    if (!isOpen()) return -1;

    QByteArray resp = sendRecvPacket(createCommandBuffer(AnalogRead, &pin, 1));
    uint16_t value = *((uint16_t*)(resp.data()+1));
    return value;
}

void DeviceInterface::analogWrite(uint8_t pin, uint8_t value, int *errCode)
{
    handleErrCode(errCode);
    if (!isOpen()) return;

    QByteArray args(2, 0);
    args[OFFSET_ARG_PIN]=pin;
    args[OFFSET_ARG_VALUE]=value;
    QByteArray resp = sendRecvPacket(createCommandBuffer(AnalogWrite,args));
    if (resp.isEmpty() && errCode)
        *errCode=UnknownError;
}

QPair<float, float> DeviceInterface::getDataFromDHT(uint8_t pin, int *errCode)
{
    QPair<float, float> result;
    handleErrCode(errCode);
    if(!isOpen())
        return result;
    QByteArray args(2,0);
    args[0]=pin;
    args[1]=11;
    QByteArray resp = sendRecvPacket(createCommandBuffer(GetDataFromDHT, args));
    result.first = *((float*)(resp.data()+1));
    result.second = *((float*)(resp.data()+5));
    return result;
}

QList<float> DeviceInterface::getBarometerData(int *errCode)
{
    QList<float> result;
    handleErrCode(errCode);
    if(!isOpen())
        return result;

    QByteArray resp = sendRecvPacket(createCommandBuffer(GetBarometerData));
    for(int i=0; i<3; i++) {
        result.append(*(float*)(resp.data()+(1+i*4)));
    }
    return result;
}

QList<PinState> DeviceInterface::getPinConfig(int *errCode)
{
    QList<PinState> result;
    const int pinsCount = PINCOUNT;
    handleErrCode(errCode);
    if (!isOpen()) return result;
    QByteArray response = sendRecvPacket(createCommandBuffer(GetPinConfig));

    for (int i=0; i<pinsCount; i++){
        PinState state = fromByte(response[i+1]);
        result.append(state);
    }
    return result;
}

void DeviceInterface::setPinConfig(const QList<PinState> &states, int *errCode)
{
    handleErrCode(errCode);
    if (!isOpen()) return;

    QByteArray args(PINCOUNT,0);
    for (int i=0; i<PINCOUNT; i++){
        args[i] = pinStateToByte(states[i]);
    }
    sendRecvPacket(createCommandBuffer(SetPinConfig,args));
}

QList<I2CDevice> DeviceInterface::getI2CdevicesList(int *errCode)
{
    QList<I2CDevice> result;
    handleErrCode(errCode);
    if(!isOpen())
        return result;

    QByteArray response = sendRecvPacket(createCommandBuffer(GetI2CDevicesList));
    for(int i=1; i<=62 && response[i]!='\0'; result.append((I2CDevice)(uint8_t)response[i++]));
    return result;
}

QByteArray DeviceInterface::createCommandBuffer(CommandsEnum command, const QByteArray &arguments) const
{
    if (arguments.size()>(PACKETSIZE-2)) return QByteArray();
    QByteArray buf(PACKETSIZE, 0);
    buf[OFFSET_CMD]=command;
    buf.replace(OFFSET_ARG, arguments.size(), arguments);
    if (buf.size()!= PACKETSIZE) {
        qDebug() << "my error";
        return QByteArray();
    }
    buf[OFFSET_VER]=PROTOCOLVERSION;
    return buf;
}

QByteArray DeviceInterface::createCommandBuffer(CommandsEnum command, const uint8_t *args, uint8_t argsLength) const
{
    return createCommandBuffer(command, QByteArray((char*)args,argsLength));
}

void DeviceInterface::handleErrCode(int *errCode) const
{
    if (errCode != NULL) {
        if (!isOpen()){
            *errCode = DeviceNotOpenedError;
        } else {
            *errCode = NoError;
        }
    }
}
