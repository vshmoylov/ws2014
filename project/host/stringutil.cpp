#include "stringutil.h"


QString StringUtil::i2cDeviceName(I2CDevice q)
{
    switch(q) {
    case Barometer:
        return "Barometer";
    }
    return "Unknown";
}

QString StringUtil::pinDevIdToString(DevId id){
    switch (id){
    case NoDevice:
        return "NoDevice";
    case Led:
        return "Led";
    case Button:
        return "Button";
    case Switch:
        return "Switch";
    case RotaryAngle:
        return "RotaryAngle";
    case Touch:
        return "Touch";
    case MagneticSwitch:
        return "MagneticSwitch";
    case LightSensor:
        return "LightSensor";
    case SoundSensor:
        return "SoundSensor";
    case TemperatureSensor:
        return "TemperatureSensor";
    case VibrationMotor:
        return "VibrationMotor";
    case Buzzer:
        return "Buzzer";
    case Relay:
        return "Relay";
    case ServoMotor:
        return "ServoMotor";
    case Lcd:
        return "Lcd";
    case FourDigitDisplay:
        return "FourDigitDisplay";
    case HumiditySensor:
        return "HumiditySensor";
    case MotionSensor:
        return "MotionSensor";
    }
    return "Unknown";
}

QString StringUtil::pinIdToString(Pins pin)
{
    switch (pin){
    case PinD0:  return "PinD0";
    case PinD1:  return "PinD1";
    case PinD2:  return "PinD2";
    case PinD3:  return "PinD3";
    case PinD4:  return "PinD4";
    case PinD5:  return "PinD5";
    case PinD6:  return "PinD6";
    case PinD7:  return "PinD7";
    case PinD8:  return "PinD8";
    case PinD9:  return "PinD9";
    case PinD10: return "PinD10";
    case PinD11: return "PinD11";
    case PinD12: return "PinD12";
    case PinD13: return "PinD13";
    case PinA0:  return "PinA0";
    case PinA1:  return "PinA1";
    case PinA2:  return "PinA2";
    case PinA3:  return "PinA3";
    case PinA4:  return "PinA4";
    case PinA5:  return "PinA5";
    case PinA6:  return "PinA6";
    case PinA7:  return "PinA7";
    }
    return "Unknown";
}

int StringUtil::i2cDeviceValuesCount(I2CDevice i2c)
{
    switch(i2c) {
    case Barometer:
        return 3;
        break;
    }
    return 0;
}

QStringList StringUtil::getI2CDeviceChildrenNames(I2CDevice i2c)
{
    switch(i2c) {
    case Barometer:
        return QStringList() <<
            QString::fromUtf8("Температура") <<
            QString::fromUtf8("Давление") <<
            QString::fromUtf8("Высота");
    }
    return QStringList();
}
