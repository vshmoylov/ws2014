#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "types.h"
#include "deviceethernetinterface.h"
#include "devicecominterface.h"
#include "stringutil.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnWidth(0, 150);
    i2cTimer = new QTimer;


    ui->tableWidget->setRowCount(ui->spinBoxTableRowCount->value());
    for(int i=0; i<ui->spinBoxTableRowCount->value(); i++) {
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(StringUtil::pinIdToString((Pins)i)));
        pinType.push_back(new QComboBox());
        for(uint8_t j=0; j<LastDevice; j++)
            pinType[i]->addItem(StringUtil::pinDevIdToString(j));
        ui->tableWidget->setCellWidget(i, 1, pinType[i]);

        output.push_back(new QCheckBox());
        ui->tableWidget->setCellWidget(i, 2, output[i]);

        analog.push_back(new QCheckBox());
        ui->tableWidget->setCellWidget(i, 3, analog[i]);
    }

    ui->comPortConnectionWidget->setVisible(false);
//    ui->tabWidget->removeTab(6);
    ui->tabWidget->setEnabled(false);
    device=NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete device;
}

void MainWindow::on_openButton_clicked()
{
    if (device && device->isOpen()){
        ui->tabWidget->setEnabled(false);
        i2cTimer->stop();
        device->disconnect();
        ui->openButton->setText(QString::fromUtf8("Подключиться"));
        ui->connectionWidget->setEnabled(true);
        delete device;
        device=NULL;
    } else {
        if (ui->connectionTypeComboBox->currentIndex()){
            device = new DeviceComInterface();
            ((DeviceComInterface*)device)->connect(ports[ui->comPortComboBox->currentIndex()]);
        } else {
            device = new DeviceEthernetInterface();
            ((DeviceEthernetInterface*)device)->connect(ui->ipLineEdit->text(), ui->portLineEdit->text().toInt());
        }
        ui->tabWidget->setEnabled(true);
        ui->openButton->setText(QString::fromUtf8("Отключиться"));
        ui->connectionWidget->setEnabled(false);
        getDeviceConfig();

        on_tabWidget_currentChanged(ui->tabWidget->currentIndex());
    }
}

uint8_t pinStateToByte(PinState state) {
    return ((state.id & 0b00111111) << 2) | ((state.analog & 1) << 1) | (state.output & 1);
}

PinState fromByte(uint8_t value){
    PinState state;
    state.id = value >> 2;
    state.analog = (value > 1)&1;
    state.output = value & 1;
    return state;
}

void MainWindow::getDeviceConfig()
{
    devConfig.clear();
    devConfig=device->getPinConfig();
    ui->plainTextEdit->clear();
    for (int i=0; i<PINCOUNT; i++){
        PinState state = devConfig[i];
        ui->plainTextEdit->appendPlainText(QString("%1 %2 %3").arg(StringUtil::pinDevIdToString(state.id)).arg(state.analog?"analog":"digital").arg(state.output?"output":"input"));
    }

    for(int i=0; i<devConfig.size(); i++) {
        pinType[i]->setCurrentIndex(devConfig[i].id);
        output[i]->setChecked(devConfig[i].output);
        analog[i]->setChecked(devConfig[i].analog);
    }
}

void MainWindow::on_pushButton_clicked()
{
    uint8_t pin = ui->spinBox->value();
    device->digitalWrite(pin, ui->pushButton->isChecked());
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    switch (index){
    case 0:
        break;
    case 1:
        on_pushButtonAnalogActuatorsRefresh_clicked();
        break;
    case 2:
        on_analogDataRefreshButton_clicked();
        break;
    case 3:
        break;
    case 5:
        int w = ui->tableWidget->width();
        int c = ui->tableWidget->columnCount();
        for(int i=0; i<c; i++) {
            ui->tableWidget->setColumnWidth(i, w/c-5);
        }
        break;
    }
}

double getCelsiusTemp(int pinValue){
  double val = pinValue;
  double resistance = (double)(1023 - val)*10000/val;
  double B = 3975;
  return 1/(log(resistance/10000)/B+1/298.15)-273.15;
}

void MainWindow::on_analogDataRefreshButton_clicked()
{
    ui->analogTreeWidget->clear();
    for (int i=0; i<devConfig.size(); i++){
        PinState state = devConfig.at(i);
        if (state.id && state.analog==true && state.output==false){
            QString value;

            switch (state.id){
            case TemperatureSensor:
                value = QString::number(getCelsiusTemp(device->analogRead(i)));
                break;
            case HumiditySensor:
                {
                    QPair<float, float> values = device->getDataFromDHT(i);
                    value = QString::fromUtf8("Влажность: %1; Температура: %2").arg(values.first).arg(values.second);
                }
                break;
            default:
                value = QString::number(device->analogRead(i));
            }

            ui->analogTreeWidget->addTopLevelItem(new QTreeWidgetItem(QStringList()
                                                                      << QString("PIN%1").arg(i)
                                                                      << StringUtil::pinDevIdToString(state.id)
                                                                      << value));
        }
    }
    ui->analogLcdNumber->display(ui->analogTreeWidget->topLevelItemCount());
}

void MainWindow::on_pushButton_4_clicked()
{
    devConfig.clear();

    for(int i=0; i<ui->tableWidget->rowCount(); i++) {
        PinState tmp;
        tmp.id = pinType[i]->currentIndex();
        tmp.output = output[i]->isChecked();
        tmp.analog = analog[i]->isChecked();
        devConfig.append(tmp);
    }
    device->setPinConfig(devConfig);
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->tableWidget->setRowCount(ui->spinBoxTableRowCount->value());
}

void MainWindow::on_pushButton_5_clicked()
{
    getDeviceConfig();
}

void MainWindow::on_pushButton_6_clicked()
{
    device->analogWrite(ui->spinBox->value(),ui->spinBox_2->value());
}

void MainWindow::on_pushButton_7_clicked()
{
    QList<I2CDevice> i2cDevices = device->getI2CdevicesList();
    ui->treeWidget->clear(); // в каком порядке удалять и нужно ли delete все указатели
    for(int i=0; i<i2cDevices.size(); i++) {
        QTreeWidgetItem* tmp = new QTreeWidgetItem(QStringList() << StringUtil::i2cDeviceName(i2cDevices[i]) << "" <<QString::number(i2cDevices[i]));
        ui->treeWidget->addTopLevelItem(tmp);
        addI2CDeviceWidgetChilds(tmp, i2cDevices[i]);
    }
}

void MainWindow::addI2CDeviceWidgetChilds(QTreeWidgetItem *a, I2CDevice i2c)
{
    int cnt = StringUtil::i2cDeviceValuesCount(i2c);
    QStringList children = StringUtil::getI2CDeviceChildrenNames(i2c);
    for(int i=0; i<cnt; i++) {
        a->addChild(new QTreeWidgetItem(QStringList() << children[i]));
    }
}

void MainWindow::on_treeWidget_itemExpanded(QTreeWidgetItem *item)
{
    switch(item->data(2, 0).toInt()) {
    case Barometer:
        QList<float> barometerData = device->getBarometerData();
        for(int j=0; j<3; j++)
            item->child(j)->setData(1, 0, barometerData[j]);
        break;
    }

    i2cTimer->start(1000);
    connect(i2cTimer, SIGNAL(timeout()), this, SLOT(refreshI2CDevicesData()));
}

void MainWindow::refreshI2CDevicesData()
{
    int expandedCount = 0;
    for(int i=0; i<ui->treeWidget->topLevelItemCount(); i++) {
        QTreeWidgetItem *currentItem = ui->treeWidget->topLevelItem(i);
        if(currentItem->isExpanded()) {
            expandedCount++;
            switch(currentItem->data(2, 0).toInt()) {
            case Barometer:
                QList<float> barometerData = device->getBarometerData();
                for(int j=0; j<3; j++)
                    currentItem->child(j)->setData(1, 0, barometerData[j]);
                break;
            }
        }
    }
    if(expandedCount == 0)
        i2cTimer->stop();
}


// dat spike
QVector<QPushButton*> highBtns(PINCOUNT, NULL);
QVector<QPushButton*> lowBtns(PINCOUNT, NULL);

void MainWindow::setHighVoltage()
{
    QPushButton* sndr = qobject_cast<QPushButton*>(sender());
    if(sndr) {
        for(int i=0; i<highBtns.size(); i++) {
            if(highBtns[i] == sndr)
                device->digitalWrite(i, device->HIGH);
        }
    }
}

void MainWindow::setLowVoltage()
{
    QPushButton* sndr = qobject_cast<QPushButton*>(sender());
    if(sndr) {
        for(int i=0; i<lowBtns.size(); i++) {
            if(lowBtns[i] == sndr)
                device->digitalWrite(i, device->LOW);
        }
    }
}

void MainWindow::on_pushButtonAnalogActuatorsRefresh_clicked()
{
    for(int i=0; i<highBtns.size(); i++) {
        if(highBtns[i] != NULL)
            delete highBtns[i];
        if(lowBtns[i] != NULL)
            delete lowBtns[i];
    }
    ui->tableWidgetDigitalActuators->clearContents();

    int currentRow = 0;
    for(int i=0; i<devConfig.size(); i++) {
        PinState current = devConfig[i];
        if(current.id!=0 && !current.analog && current.output) {
            ui->tableWidgetDigitalActuators->setRowCount(currentRow+1);
            ui->tableWidgetDigitalActuators->setItem(currentRow, 0, new QTableWidgetItem(StringUtil::pinIdToString((Pins)i)));
            ui->tableWidgetDigitalActuators->setItem(currentRow, 1, new QTableWidgetItem(StringUtil::pinDevIdToString(current.id)));

            QPushButton *highBtn = new QPushButton("HIGH");
            ui->tableWidgetDigitalActuators->setCellWidget(currentRow, 2, highBtn);
            highBtns[i] = highBtn;
            connect(highBtn, SIGNAL(clicked()), this, SLOT(setHighVoltage()));

            QPushButton *lowBtn = new QPushButton("LOW");
            ui->tableWidgetDigitalActuators->setCellWidget(currentRow, 3, lowBtn);
            lowBtns[i] = lowBtn;
            connect(lowBtn, SIGNAL(clicked()), this, SLOT(setLowVoltage()));
        }
    }
}

void MainWindow::on_connectionTypeComboBox_currentIndexChanged(int index)
{
    ui->ethernetConnectionWidget->setVisible(!index);
    ui->comPortConnectionWidget->setVisible(index);
    if (index){
        on_comPortsRefreshPushButton_clicked();
    } else {
    }
}

void MainWindow::on_digitalDataRefreshPushButton_clicked()
{
}

void MainWindow::on_comPortsRefreshPushButton_clicked()
{
    ui->comPortComboBox->clear();
    ports.clear();
    ports = QSerialPortInfo::availablePorts();
    foreach (const QSerialPortInfo &info, ports) {
        ui->comPortComboBox->addItem(info.portName()+": "+info.description() );
    }
}
