#include "deviceethernetinterface.h"

DeviceEthernetInterface::DeviceEthernetInterface():DeviceInterface()
{
}

DeviceEthernetInterface::~DeviceEthernetInterface()
{
    if (socket.isOpen())
        socket.close();
}

void DeviceEthernetInterface::connect(const QHostAddress &address, quint16 port)
{
    socket.connectToHost(address, port);
    socket.waitForConnected(5000);
}

void DeviceEthernetInterface::connect(const QString &hostName, quint16 port)
{
    socket.connectToHost(hostName, port);
    socket.waitForConnected(5000);
}

void DeviceEthernetInterface::disconnect()
{
    if (socket.isOpen())
        socket.close();
}

bool DeviceEthernetInterface::isOpen() const
{
    return socket.isOpen();
}

QByteArray DeviceEthernetInterface::sendRecvPacket(QByteArray buf)
{
    QByteArray result;
    if (!isOpen()) return result;
    if (buf.size()!=PACKETSIZE) return result;

    socket.write(buf);
    socket.waitForBytesWritten(READWRITETIMEOUT);

    QTime startTime= QTime::currentTime();
    do {
        socket.waitForReadyRead(READWRITETIMEOUT);
        result += socket.readAll();
    } while (result.size()<PACKETSIZE
             && abs(QTime::currentTime().msecsTo(startTime)) < READWAITTIME);

    if (result.size()!=PACKETSIZE) result.clear();
    return result;
}
