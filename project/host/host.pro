#-------------------------------------------------
#
# Project created by QtCreator 2014-02-03T14:14:48
#
#-------------------------------------------------

QT       += core gui network

CONFIG += serialport
qtAddLibrary(QtSerialPort)

TARGET = host
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    deviceinterface.cpp \
    devicecominterface.cpp \
    deviceethernetinterface.cpp \
    stringutil.cpp

HEADERS  += mainwindow.h \
    types.h \
    deviceinterface.h \
    devicecominterface.h \
    deviceethernetinterface.h \
    stringutil.h

FORMS    += mainwindow.ui
