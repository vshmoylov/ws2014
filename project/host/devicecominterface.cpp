#include "devicecominterface.h"

DeviceComInterface::DeviceComInterface():DeviceInterface()
{
}

DeviceComInterface::~DeviceComInterface()
{
    if (comPort.isOpen())
        comPort.close();
}

void DeviceComInterface::connect(const QSerialPortInfo &info)
{
    if (comPort.isOpen()) comPort.close();
    comPort.setPort(info);
    comPort.open(QIODevice::ReadWrite);
    comPort.setBaudRate(SERIALSPEED);
}

void DeviceComInterface::disconnect()
{
    if (comPort.isOpen())
        comPort.close();
}

bool DeviceComInterface::isOpen() const
{
    return comPort.isOpen();
}

QByteArray DeviceComInterface::sendRecvPacket(QByteArray buf)
{
    QByteArray result;
    if (!isOpen()) return result;
    if (buf.size()!=PACKETSIZE) return result;
//    currentPort.clear();
    comPort.write(buf);
    comPort.waitForBytesWritten(READWRITETIMEOUT);
    QTime startTime= QTime::currentTime();
    do {
        comPort.waitForReadyRead(READWRITETIMEOUT);
        result += comPort.readAll();
    } while (result.size()<PACKETSIZE
             && abs(QTime::currentTime().msecsTo(startTime)) < READWAITTIME);
    if (result.size()!=PACKETSIZE) result.clear();
    return result;
}
