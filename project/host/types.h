#ifndef TYPES_H
#define TYPES_H

#define SERIALSPEED 9600 //115200
#define PACKETSIZE 64
#define PINCOUNT 24

#define OFFSET_CMD 0
#define OFFSET_ARG 1
#define OFFSET_ARG_PIN 0
#define OFFSET_ARG_VALUE 1
#define OFFSET_VER (PACKETSIZE-1)

enum Pins {
    PinD0  =  0,
    PinD1  =  1,
    PinD2  =  2,
    PinD3  =  3,
    PinD4  =  4,
    PinD5  =  5,
    PinD6  =  6,
    PinD7  =  7,
    PinD8  =  8,
    PinD9  =  9,
    PinD10 = 10,
    PinD11 = 11,
    PinD12 = 12,
    PinD13 = 13,
    PinA0  = 14,
    PinA1  = 15,
    PinA2  = 16,
    PinA3  = 17,
    PinA4  = 18,
    PinA5  = 19,
    PinA6  = 20,
    PinA7  = 21
};

enum CommandsEnum { None=0,
                    DigitalRead=1,
                    DigitalWrite=2,
                    AnalogRead=3,
                    AnalogWrite=4,
                    EnumerateI2C=5,
                    LCDPrint=6,//not implemented
                    GetState=7,//reserved
                    GetPinConfig=8,
                    SetPinConfig=9,
                    RSPOk=10,
                    RSPErr=11,
                    GetDataFromDHT=12,
                    GetI2CDevicesList=13,
                    GetBarometerData=14
                  };

enum DevId
{
    NoDevice = 0,
    Led=1,
    Button=2,
    Switch=3,
    RotaryAngle=4,
    Touch=5,
    MagneticSwitch=6,
    LightSensor=7,
    SoundSensor=8,
    TemperatureSensor=9,
    VibrationMotor=10,
    Buzzer=11,
    Relay=12,
    ServoMotor=13,
    Lcd=14,
    FourDigitDisplay=15,
    HumiditySensor=16,
    MotionSensor=17,
    LastDevice
};

enum I2CDevice {
    Barometer=119
};

struct PinState {
    uint8_t id:6;
    uint8_t analog:1;
    uint8_t output:1;
    PinState():id(0),analog(0),output(0){}
    PinState(DevId devId, bool isAnalog, bool isOutput):id(devId), analog(isAnalog), output(isOutput){}
};

uint8_t pinStateToByte(PinState state);
PinState fromByte(uint8_t value);

#endif // TYPES_H
