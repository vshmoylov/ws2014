#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <QString>
#include <QStringList>
#include "types.h"

class StringUtil
{
public:
    static QString i2cDeviceName(I2CDevice q);
    inline static QString pinDevIdToString(uint8_t id){
        return pinDevIdToString((DevId)id);
    }
    static QString pinDevIdToString(DevId id);
    static QString pinIdToString(Pins pin);
    static int i2cDeviceValuesCount(I2CDevice i2c);
    static QStringList getI2CDeviceChildrenNames(I2CDevice i2c);
};

#endif // STRINGUTIL_H
