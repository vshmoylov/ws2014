#ifndef BIGSERIALLCD_H
#define BIGSERIALLCD_H

#include <SerialLCD.h>

#define ROWS_COUNT 6

class BigSerialLCD : public SerialLCD {
public:
    uint8_t buf[ROWS_COUNT][16];
    uint8_t currentRow;
    uint8_t currentColumn;
    uint8_t currentVisible;
    char* floatToString(char *outstr, float value, int places, int minwidth=0, bool rightjustify=false);

public:
    BigSerialLCD(int a, int b) : SerialLCD(a, b), currentVisible(0), 
                                 currentRow(0), currentColumn(0) {
        for(uint8_t i=0; i<ROWS_COUNT; i++)
            memset(buf[i], ' ', 16);
    }
    BigSerialLCD(const BigSerialLCD &a) : SerialLCD(a), currentVisible(a.currentVisible), 
                                          currentRow(a.currentRow), currentColumn(a.currentColumn) {                                 
        for(uint8_t i=0; i<ROWS_COUNT; i++)
            for(uint8_t j=0; j<16; j++)
                buf[i][j] = a.buf[i][j];
    }
    
    bool setCursor(uint8_t column, uint8_t row);
    void print(uint8_t b);
    void print(const char[]);
    void print(unsigned long n, uint8_t base);
    void print(const float f, const uint8_t prec);
    void scrollDisplayUp();
    void scrollDisplayDown();
    //void clear();
};

#endif