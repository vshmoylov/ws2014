#include "BigSerialLCD.h"

bool BigSerialLCD::setCursor(uint8_t column, uint8_t row) {
    //if(row <= 1) {
    //    SerialLCD::setCursor(column, row);
    //}
    if(row >= ROWS_COUNT || column >= 16) 
        return false;
    currentRow = row;
    currentColumn = column;
    return true;
}

void BigSerialLCD::print(uint8_t b) {
    buf[currentRow][currentColumn] = b; // закидываем в буфер
    
    if(currentRow == currentVisible || currentRow == currentVisible+1) {
        if(currentRow == currentVisible)
            SerialLCD::setCursor(currentColumn, 0);
        else
            SerialLCD::setCursor(currentColumn, 1);
        SerialLCD::print(b);
    }
    
    currentColumn++;
    if(currentColumn >= 16) {
        currentColumn = 0;
        currentRow++;
    }
}

void BigSerialLCD::print(const char toPrint[]) {
    bool canPrint = true;
    if(currentRow == currentVisible || currentRow == currentVisible+1) {
        if(currentRow == currentVisible)
            SerialLCD::setCursor(currentColumn, 0);
        else
            SerialLCD::setCursor(currentColumn, 1);
    } else {
        canPrint = false;
    }
            
    for(uint8_t i=0; toPrint[i]; i++) { // todo оптимизировать, попробовать memcpy
        buf[currentRow][currentColumn] = toPrint[i];
        if(canPrint)
            SerialLCD::print(toPrint[i]);
        
        currentColumn++;
        if(currentColumn >= 16) {
            currentColumn = 0;
            currentRow++;
            
            if(currentRow == currentVisible+1)
                SerialLCD::setCursor(0, 1);
            else
                canPrint = false;
        }
    }
}

void BigSerialLCD::print(unsigned long n, uint8_t base) {
    unsigned char buf[8 * sizeof(long)]; // Assumes 8-bit chars.
    unsigned long i = 0;

    if(base == 0)
        print(n);
    else if(base!=0) {
        if (n == 0) {
            print('0');
            return;
        }

        while (n > 0) {
            buf[i++] = n % base;
            n /= base;
        }

        for (; i > 0; i--)
            print((char) (buf[i - 1] < 10 ?
                          '0' + buf[i - 1] :
                          'A' + buf[i - 1] - 10));
    }
}

void BigSerialLCD::print(const float f, const uint8_t prec) {
    char buf[16] = {0};
    
    floatToString(buf, f, prec);
    
    // print integer part
    //uint8_t i, j = 15, mod;
    //long ff = (int)f;
    //while(true) {
    //    if((mod = ff % 10) == 0)
    //        break;
    //    buf[j--] = '0' + mod;
    //    ff /= 10;
    //}
    
    // move integer part
    //for(i=0; j+i+1!=16; i++) {
    //    buf[i] = buf[j+i+1];
    //    buf[j+i+1] = 0;
    //}
    
    // print floating part
    //buf[i++] = '.';
    //int div = 10;
    //float fp = f - (float)((int)f);
    //for(j=0; j<prec; j++, i++) {
    //    buf[i] = '0' + ((int)(fp * div) % 10);
    //    div *= 10;
    //}
    
    // print what we got
    //buf[i] = 0;
    print(buf);
}
    
void BigSerialLCD::scrollDisplayUp() {
    if(currentVisible == 0)
        return;
    currentVisible--;
    
    for(uint8_t i=0; i<2; i++) {
        SerialLCD::setCursor(0,i);
        for(uint8_t j=0; j<16; j++) {
            //if(buf[currentVisible+i+1][j] != buf[currentVisible+i][j])
                SerialLCD::print(buf[currentVisible+i][j]);
        }   
    }
}

void BigSerialLCD::scrollDisplayDown() {
    if(currentVisible >= ROWS_COUNT-1)
        return;
    currentVisible++;
    
    for(uint8_t i=0; i<2; i++) {
        SerialLCD::setCursor(0,i);
        for(uint8_t j=0; j<16; j++) {
            //if(i == 0) {
            //    if(buf[currentVisible-1+i][j] != buf[currentVisible+i][j])
                    SerialLCD::print(buf[currentVisible+i][j]);
            //} else {
            //    SerialLCD::print(buf[currentVisible+i][j]);
            //}
        }
    }
}
/*
void BigSerialLCD::clear() {
    for(uint8_t i=0; i<ROWS_COUNT; i++) {
        memset(buf, ' ', 16);
    }
    currentRow = 0;
    currentColumn = 0;
    currentVisible = 0;
    
    BigSerialLCD::clear();
}
*/
char* BigSerialLCD::floatToString(char *outstr, float value, int places, int minwidth, bool rightjustify) {
    // this is used to write a float value to string, outstr.  oustr is also the return value.
    int digit;
    float tens = 0.1;
    int tenscount = 0;
    int i;
    float tempfloat = value;
    int c = 0;
    int charcount = 1;
    int extra = 0;
    // make sure we round properly. this could use pow from <math.h>, but doesn't seem worth the import
    // if this rounding step isn't here, the value  54.321 prints as 54.3209

    // calculate rounding term d:   0.5/pow(10,places)  
    float d = 0.5;
    if (value < 0)
        d = -d;
    // divide by ten for each decimal place
    for (i = 0; i < places; i++)
        d/= 10.0;    
    // this small addition, combined with truncation will round our values properly 
    tempfloat +=  d;

    // first get value tens to be the large power of ten less than value    
    if (value < 0)
        tempfloat *= -1.0;
    while ((tens * 10.0) <= tempfloat) {
        tens *= 10.0;
        tenscount += 1;
    }

    if (tenscount > 0)
        charcount += tenscount;
    else
        charcount += 1;

    if (value < 0)
        charcount += 1;
    charcount += 1 + places;

    minwidth += 1; // both count the null final character
    if (minwidth > charcount){        
        extra = minwidth - charcount;
        charcount = minwidth;
    }

    if (extra > 0 and rightjustify) {
        for (int i = 0; i< extra; i++) {
            outstr[c++] = ' ';
        }
    }

    // write out the negative if needed
    if (value < 0)
        outstr[c++] = '-';

    if (tenscount == 0) 
        outstr[c++] = '0';

    for (i=0; i< tenscount; i++) {
        digit = (int) (tempfloat/tens);
        itoa(digit, &outstr[c++], 10);
        tempfloat = tempfloat - ((float)digit * tens);
        tens /= 10.0;
    }

    // if no places after decimal, stop now and return
    // otherwise, write the point and continue on
    if (places > 0)
    outstr[c++] = '.';


    // now write out each decimal place by shifting digits one by one into the ones place and writing the truncated value
    for (i = 0; i < places; i++) {
        tempfloat *= 10.0; 
        digit = (int) tempfloat;
        itoa(digit, &outstr[c++], 10);
        // once written, subtract off that digit
        tempfloat = tempfloat - (float) digit; 
    }
    if (extra > 0 and not rightjustify) {
        for (int i = 0; i< extra; i++) {
            outstr[c++] = ' ';
        }
    }

    outstr[c++] = '\0';
    return outstr;
}