int ledPin = 13; // standart led

void setup() 
{
  // create Serial Object
  Serial.begin(9600);
  
  pinMode(ledPin, OUTPUT);
}

void loop() 
{
  
  // Have the arduino wait to receive input 
  while ( Serial.available() == 0 );
  
  // Read the Input 
  int val = Serial.read() - '0';
  
  if ( val == 1 ) 
  {
    Serial.print("Led is On ");
    digitalWrite(ledPin, HIGH);
  }
  else if ( val == 0)
  {
    Serial.print("Led is Off ");
    digitalWrite(ledPin, LOW);
  }
  else 
  {
    Serial.print("Error input data ");
  }
  
  // Echo the input 
  Serial.println(val);
  
  Serial.flush(); // не работает очистка - продолжает выводить рулон
    
}
