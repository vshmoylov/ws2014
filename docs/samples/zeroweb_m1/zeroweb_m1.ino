// zeroweb with analog input measurements v.1 20130502

// Make sure to do these steps before connecting and uploading code:

// 1) if powered by power connector, USE POSITIVE POLARITY IN CENTER 
// 2) SELECT 3.3V on foca 2.2
// 3) select Arduino Duemilanove with ATmega328

#include <SPI.h>
#include <Ethernet.h>
#include <math.h>
#include "DHT.h"

/// analog temperature measurement device handling ///

int atmp_tMeasured;
float atmp_tC;
float atmp_tF;

int atmp_B=3975; 
float atmp_resistance;

void temperatureMeasure(int numPin){
  atmp_tMeasured = analogRead(numPin);
  atmp_resistance = (float)(1023-atmp_tMeasured)*10000/atmp_tMeasured;
  atmp_tC = 1/(log(atmp_resistance/10000)/atmp_B+1/298.15)-273.15;
  atmp_tF = atmp_tC*9/5+32;
}

float temperatureGetAsC(){
  return atmp_tC;
}

float temperatureGetAsF(){
  return atmp_tF;
}

/// analog luminosity measurement device handling ///

int alum_lMeasured;

void luminosityMeasure(int numPin){
  alum_lMeasured = analogRead(numPin);
}

int luminosityGet(){
  return alum_lMeasured;
}

/// dht humidity measurement ///

#define DHTTYPE DHT11   // DHT 11 
#define DHTPIN 10

DHT dht(DHTPIN, DHTTYPE);

float dht_humidity;
float dht_temperature;

void dht_measure(){
  dht_humidity = dht.readHumidity();
  dht_temperature = dht.readTemperature();
}

int dht_isHumidityValid(){
  return !isnan(dht_humidity);
}

int dht_isTemperatureValid(){
  return !isnan(dht_temperature);
}

float dht_getHumidity(){
  return dht_humidity;
}

float dht_getTemperature(){
  return dht_temperature;
}

/// network handling ///

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,0, 88);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
}

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connnection: close");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
                    // add a meta refresh tag, so the browser pulls again every 5 seconds:
          client.println("<meta http-equiv=\"refresh\" content=\"1\">");

          client.println("<title>Zuzuz weather station</title>");
          client.println("<h1>Zuzuz weather station</h1>");
          client.println("<h3>ver 0.01 (c) 2013 Kisa, Masha, Kujan</h3>");
          
          client.println("<h2><pre>");

          temperatureMeasure(0);
          client.println("temperature:");
          client.print("     ");
          client.print(temperatureGetAsC());
          client.println(" C,");
          client.print("     ");
          client.print(temperatureGetAsF());
          client.println(" F;");

          luminosityMeasure(1);
          client.println("luminosity:");
          client.print("     ");
          client.print(luminosityGet());
          client.println(" relative units;\n");
           
          client.println("</pre></h2></html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(2);
    // close the connection:
    client.stop();
  }
}

