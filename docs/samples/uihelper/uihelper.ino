/*
 board: arduino leonardo, 5V
 
 pins:
 D13 (13) - ir sesor
*/

#include <IRSendRev.h>

#define MEHAMURO1_RECON1_IRCODE_TABLESIZE 16
#define MEHAMURO1_RECON1_CTRLDEV_QTY 3

#define BTN_CODE_UNKNOWN 255

unsigned char ir_codes[MEHAMURO1_RECON1_IRCODE_TABLESIZE][11] = {
 {9, 177, 89, 11, 33, 4, 0, 253, 48, 207, 0}, /* black noname irc */
 {9, 176, 91, 10, 34, 4, 0, 253, 8, 247, 1},
 {9, 178, 89, 11, 33, 4, 0, 253, 136, 119, 2},
 {9, 177, 89, 11, 33, 4, 0, 253, 72, 183, 3},

 {9, 181, 88, 11, 33, 4, 0, 255, 104, 151, 0}, /* silver noname irc */
 {9, 178, 90, 10, 34, 4, 0, 255, 48, 207, 1},
 {9, 178, 89, 11, 33, 4, 0, 255, 24, 231, 2},
 {9, 178, 90, 11, 32, 4, 0, 255, 122, 133, 3},

 {9, 178, 89, 11, 32, 4, 158, 97, 8, 247, 0}, /* yamaha cdc1 vv27510 */
 {9, 178, 89, 11, 32, 4, 158, 97, 136, 119, 1},
 {9, 178, 89, 11, 32, 4, 158, 97, 72, 183, 2},
 {9, 178, 89, 11, 32, 4, 158, 97, 200, 55, 3},
 
 {9, 178, 88, 11, 32, 4, 0, 255, 130, 125, 0}, /* xoro 200p */
 {9, 178, 89, 11, 33, 4, 0, 255, 144, 111, 1},
 {9, 178, 88, 11, 32, 4, 0, 255, 160, 95, 2},
 {9, 178, 88, 11, 33, 4, 0, 255, 128, 127, 3}
};

unsigned char ir_dta[20];

unsigned char cmp_ir_code(unsigned char *c1, unsigned char *c2){
 unsigned char i;
 
 for(i = 5; i < 10; i++) /* the first five codes appear to vary sometimes */
  if(c1[i] != c2[i])
   return 0;

 return 1;
}

unsigned char decode_btn_code(void){
 unsigned char i;
  
 for(i = 0; i < MEHAMURO1_RECON1_IRCODE_TABLESIZE; i++)
  if(cmp_ir_code(ir_codes[i], ir_dta))
   return ir_codes[i][10];

 return BTN_CODE_UNKNOWN;
}

void setup(){
 IR.Init(13);

 //start keyboard communication
 Keyboard.begin();
}

void loop(){
 unsigned char btn;
 
 if(IR.IsDta()){
  int length= IR.Recv(ir_dta);

  btn = decode_btn_code();
  
#ifdef MEHAMURO1_USE_COM
  Serial.print(btn);
  Serial.println();
#endif

  switch(btn){
  case 0:
    Keyboard.print("0");
    break;

  case 1:
    Keyboard.print("1");
    break;

  case 2:
    Keyboard.print("2");
    break;
  }
 }
}
